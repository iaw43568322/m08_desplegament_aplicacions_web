package curs_de_04_javaAnnotations;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
// import org.springframework.context.support.ClassPathXmlApplicationContext;

public class usJavaAnnotationsApartirDeScope {

	public static void main(String[] args) {
		
		System.out.println("-----------usJavaAnnotationsApartirDeScope - INICI -------------");
		System.out.println();
		
		// System.out.println("ABANS DE CARREGAR EL CONTEXT");
		
		// Creem el contexte carregant el fitxer xml de configuració
		// ClassPathXmlApplicationContext contexte = new ClassPathXmlApplicationContext("applicationContext.xml"); 
		// Ja no farem servir el fitxer de configuració xml
		
		// Llegir el class de configuració (que substitueix al xml)
		AnnotationConfigApplicationContext contexte = new AnnotationConfigApplicationContext(aplicacioConfig.class);
		
		// Petició de beans al contenidor Spring
		Tripulants especialistaEnECM_1 = contexte.getBean("especialistaEnECM", EspecialistaEnECM.class);
		
		System.out.println("Tripulants especialistaEnECM.agafarInforme(): " + especialistaEnECM_1.agafarInforme());
		System.out.println("Tripulants especialistaEnECM.agafarTarees(): " + especialistaEnECM_1.agafarTarees());
		
		EspecialistaEnECM especialistaEnECM_2 = contexte.getBean("especialistaEnECM", EspecialistaEnECM.class);
		
		System.out.println("EspecialistaEnECM especialistaEnECM.agafarTarees(): " + especialistaEnECM_2.agafarTarees());
		System.out.println("EspecialistaEnECM especialistaEnECM.getEmail(): " + especialistaEnECM_2.getEmail());
		System.out.println("EspecialistaEnECM especialistaEnECM.getDepartamentNom(): " + especialistaEnECM_2.getDepartamentNom());
		
		/*
		System.out.println("DESPRÉS DE CARREGAR EL CONTEXT I ABANS DE CARREGAR EL BEAN");
		
		// Petició de beans al contenidor Spring
		ControladorSondes controladorSondes = contexte.getBean("controladorSondesNau", ControladorSondes.class);
		
		System.out.println("DESPRÉS DE CARREGAR EL BEAN");
		
		System.out.println(controladorSondes.agafarInforme());
		
		System.out.println("DESPRÉS D'EXECUTAR agafarInforme()");
		
		System.out.println(controladorSondes.agafarTarees());
		
		System.out.println("DESPRÉS D'EXECUTAR agafarTarees()");
		
		
		
		// SCOPE - INICI
		
		// Petició de beans al contenidor Spring
		Tripulants doctor_1 = contexte.getBean("doctorNau", Doctor.class);
		Tripulants doctor_2 = contexte.getBean("doctorNau", Doctor.class);
		
		// Com que doctor_1 i doctor_2 tenen el patró Singleton, estaran apuntant al mateix objecte
		System.out.println("Patró Singleton:");
		System.out.println("Dir. de memòria de doctor_1: " + doctor_1);
		System.out.println("Dir. de memòria de doctor_2: " + doctor_2);
		
		// Petició de beans al contenidor Spring
		Tripulants infermera_1 = contexte.getBean("infermeraNau", Infermera.class);
		Tripulants infermera_2 = contexte.getBean("infermeraNau", Infermera.class);
		
		// Com que infermera_1 i infermera_2 tenen el patró Prototype,
		// cada cop que es crei un objecte de tipus infermera, serà un nou objecte
		System.out.println("Patró Prototype:");
		System.out.println("Dir. de memòria de infermera_1: " + infermera_1);
		System.out.println("Dir. de memòria de infermera_2: " + infermera_2);
		*/
		
		// Tanquem el contexte
		contexte.close();
		
		// System.out.println("DESPRÉS DE TANCAR EL CONTEXT");
	}

}
