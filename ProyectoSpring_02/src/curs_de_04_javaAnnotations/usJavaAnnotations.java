package curs_de_04_javaAnnotations;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class usJavaAnnotations {

	public static void main(String[] args) {
		// Creem el contexte
		ClassPathXmlApplicationContext contexte = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		// Demanem el bean al contenidor fent servir la java annotation
		Tripulants doctor = contexte.getBean("doctorNau", Doctor.class);
		Tripulants infermera = contexte.getBean("infermeraNau", Infermera.class);
		
		// Fem servir el bean
		System.out.println(doctor.agafarInforme());
		System.out.println(doctor.agafarTarees());
		
		System.out.println(infermera.agafarInforme());
		System.out.println(infermera.agafarTarees());
		/*
		System.out.println(infermera.getMantenimentMaterial());
		System.out.println(infermera.getMantenimentMedicaments());
		*/
		// Tanquem el contexte
		contexte.close();

	}

}
