package curs_de_04_javaAnnotations;

import org.springframework.stereotype.Component;

@Component
public class MantenimentInfermeriaMaterial implements MantenimentInfermeriaInterface {

	@Override
	public String ferMantenimentInfermeria() {
		return "ferMantenimentInfermeria(): manteniment del material de l'infermeria.";
	}

}
