package curs_de_04_javaAnnotations;

import org.springframework.beans.factory.annotation.Value;

public class EspecialistaEnECM implements Tripulants {
	
	// Atributs
	private InformeElectronicaInterface informeEspecialistaEnECM;
	
	@Value("${emailDepartamentECM}")
	private String email;
	
	@Value("${departamentECMNom}")
	private String departamentNom;

	/**
	 * @param informeEspecialistaEnECM
	 */
	public EspecialistaEnECM(InformeElectronicaInterface informeEspecialistaEnECM) {
		super();
		this.informeEspecialistaEnECM = informeEspecialistaEnECM;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the departamentNom
	 */
	public String getDepartamentNom() {
		return departamentNom;
	}

	/**
	 * @param departamentNom the departamentNom to set
	 */
	public void setDepartamentNom(String departamentNom) {
		this.departamentNom = departamentNom;
	}

	@Override
	public String agafarTarees() {
		return "EspecialistaEnECM: agafarTarees(): arreglar la electrònica d'Infermeria. ";
	}

	@Override
	public String agafarInforme() {
		return informeEspecialistaEnECM.getInformeElectronica();
	}
	
	// @Override
	public String ferManteniment() {
		return "EspecialistaEnECM: ferManteniment(): ";
	}

}
