package curs_de_04_javaAnnotations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan("curs_de_04_javaAnnotations")
@PropertySource("classpath:dadesNau.properties")
public class aplicacioConfig {
	// Hem creat aquesta classe per a substituir el fitxer de configuracio xml
	
	@Bean
	public InformeElectronicaInterface informeECM() {
		return new InformeECM();
	}
	
	@Bean
	public Tripulants especialistaEnECM() {
		return new EspecialistaEnECM(informeECM());
	}
}
