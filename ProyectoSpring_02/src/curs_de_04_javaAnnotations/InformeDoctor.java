package curs_de_04_javaAnnotations;

import org.springframework.stereotype.Component;

// Java Annotation
@Component
public class InformeDoctor implements InformeInfermeriaInterface {

	@Override
	public String getInformeInfermeria() {
		return "Informe d'infermeria genereat pel doctor (generat per InformeDoctor.java)";
	}

}
