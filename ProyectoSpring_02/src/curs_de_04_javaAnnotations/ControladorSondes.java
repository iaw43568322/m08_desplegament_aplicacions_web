package curs_de_04_javaAnnotations;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.stereotype.Component;

@Component("controladorSondesNau")
public class ControladorSondes implements Tripulants {
	
	// S'executa després d'haver creat el bean
	@PostConstruct
	public void inicialitzador() {
		System.out.println("ControladorSondes: S'HA EXECUTAT inicialitzador() AMB LA JAVA ANNOTATION @PostConstruct");
	}
	
	// S'executarà després de tancar el contenidor de Spring
	@PreDestroy
	public void finalitzador() {
		System.out.println("ControladorSondes: S'HA EXECUTAT finalitzador() AMB LA JAVA ANNOTATION @PreDestroy");
	}

	@Override
	public String agafarTarees() {
		return "controladorSondesNau: agafarTarees()";
	}

	@Override
	public String agafarInforme() {
		return "controladorSondesNau: agafarInforme()";
	}

}
