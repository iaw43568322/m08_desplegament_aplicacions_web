package curs_de_04_javaAnnotations;

public class InformeECM implements InformeElectronicaInterface {

	@Override
	public String getInformeElectronica() {
		return "Informe ECM (generat per InformeECM.java)";
	}

}
