package curs_de_04_javaAnnotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

// Java Annotations
@Component("infermeraNau")

@Scope("prototype")
public class Infermera implements Tripulants {
	
	// Atributs
	@Autowired
	@Qualifier("mantenimentInfermeriaMaterial")
	private MantenimentInfermeriaInterface mantenimentMaterial;
	
	@Autowired
	@Qualifier("mantInferMedica")
	private MantenimentInfermeriaInterface mantenimentMedicaments;
	
	private InformeInfermeriaInterface informeInfermera;

	/**
	 * @return the informeInfermera
	 */
	public InformeInfermeriaInterface getInformeInfermera() {
		return informeInfermera;
	}

	/**
	 * @param informeInfermera the informeInfermera to set
	 */
	@Autowired
	public void setInformeInfermera(InformeInfermeriaInterface informeInfermera) {
		this.informeInfermera = informeInfermera;
	}

	/**
	 * @return the mantenimentMaterial
	 */
	public MantenimentInfermeriaInterface getMantenimentMaterial() {
		return mantenimentMaterial;
	}

	/**
	 * @param mantenimentMaterial the mantenimentMaterial to set
	 */
	public void setMantenimentMaterial(MantenimentInfermeriaInterface mantenimentMaterial) {
		this.mantenimentMaterial = mantenimentMaterial;
	}

	/**
	 * @return the mantenimentMedicaments
	 */
	public MantenimentInfermeriaInterface getMantenimentMedicaments() {
		return mantenimentMedicaments;
	}

	/**
	 * @param mantenimentMedicaments the mantenimentMedicaments to set
	 */
	public void setMantenimentMedicaments(MantenimentInfermeriaInterface mantenimentMedicaments) {
		this.mantenimentMedicaments = mantenimentMedicaments;
	}

	@Override
	public String agafarTarees() {
		return "infermeraNau: agafarTarees(): ajudar a curar als tripulants";
	}

	@Override
	public String agafarInforme() {
		return "infermeraNau: agafarInforme(): Informe de l'infermera de la missió. " + informeInfermera.getInformeInfermeria();
	}

}
