package curs_de_04_javaAnnotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

// Creem una Java Annotation que crearà un bean de la classe Doctor.class
@Component("doctorNau")
public class Doctor implements Tripulants {
	
	// Atributs
	private InformeInfermeriaInterface informeDoctor;

	/**
	 * @param informeDoctor
	 */
	@Autowired
	public Doctor(InformeInfermeriaInterface informeDoctor) {
		super();
		this.informeDoctor = informeDoctor;
	}

	@Override
	public String agafarTarees() {
		return "doctorNau: agafarTarees(): curar als tripulants";
	}

	@Override
	public String agafarInforme() {
		return "doctorNau: agafarInforme(): Informe del doctor de la missió " + informeDoctor.getInformeInfermeria();
	}

}
