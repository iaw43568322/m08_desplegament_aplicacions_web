package curs_de_04_javaAnnotations;

import org.springframework.stereotype.Component;

@Component("mantInferMedica")
public class MantenimentInfermeriaMedicaments implements MantenimentInfermeriaInterface {

	@Override
	public String ferMantenimentInfermeria() {
		return "ferMantenimentInfermeria(): manteniment dels medicaments de l'infermeria.";
	}

}
