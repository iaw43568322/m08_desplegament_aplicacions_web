<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/recursos/css/elMeuEstil.css">
	<title>Llista de tripulants</title>
</head>
<body>
	<h1>Llista de tripulants</h1>
	
	<table>
		<tr>
			<th> ID </th>
			<th> Nom </th>
			<th> Cognom </th>
			<th> Email </th>
			<th> Data alta </th>
			<th> Departament ID </th>
			<th> Updatejar </th>
			<th> Delete </th>
		</tr>
		
		 
		<c:forEach var="tripulantTmp" items="${llistaTripulants}">
			<c:url var="linkUpdatejar" value="/tripulant/formulariUpdateTripulant">
				<c:param name="tripulantId" value="${tripulantTmp.id}"></c:param>
			</c:url>
			
			<c:url var="linkDelete" value="/tripulant/formulariDeleteTripulant">
				<c:param name="tripulantId" value="${tripulantTmp.id}"></c:param>
			</c:url>
			
			<tr>
				<td> ${tripulantTmp.id} </td>
				<td> ${tripulantTmp.nom} </td>
				<td> ${tripulantTmp.cognom} </td>
				<td> ${tripulantTmp.email} </td>
				<td> ${tripulantTmp.dataCreacio} </td>
				<td> ${tripulantTmp.departamentId} </td>
				<td>
					<a href="${linkUpdatejar}">
						<input type="button" value="Updatejar">
					</a>
				</td>
				<td>
					<a href="${linkDelete}">
						<input type="button" value="Delete" onclick="if(!(confirm('Estàs segur d'esborrar al tripulant?'))) return false">
					</a>
				</td>
			</tr>
		</c:forEach>
		
	</table>
	
	<br>
	
	<input type="button" value="Afegir tripulant a la BD"
		onclick="window.location.href='formulariInsertTripulant'; return false;"/>
		
</body>
</html>