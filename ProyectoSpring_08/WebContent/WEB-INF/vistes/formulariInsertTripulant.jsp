<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/recursos/css/elMeuEstil.css">
	<title>Insertar tripulant</title>
</head>
<body>
	<h1>Formulari per a insertar un tripulant</h1>
	
	<form:form action="procesarAltaTripulant" modelAttribute="elNouTripulant" method="POST">
		<table>
			<tr>
				<td> Nom del tripulant: </td>
				<td> <form:input path="nom"/> </td>
			</tr>
			
			<tr>
				<td> Cognom del tripulant: </td>
				<td> <form:input path="cognom"/> </td>
			</tr>
			
			<tr>
				<td> Email del tripulant: </td>
				<td> <form:input path="email"/> </td>
			</tr>
			
			<tr>
				<td> Departament (seleccionar Navegació perquè en la BD és l'únic que existeix): </td>
				<td> 
					<form:radiobutton path="departamentId" value="1" label="Navegació"/>
					<form:radiobutton path="departamentId" value="2" label="Infermeria"/>
					<form:radiobutton path="departamentId" value="3" label="Màquines"/>
					<form:radiobutton path="departamentId" value="4" label="Pont"/>
				</td>
			</tr>
			
			<tr>
				<td colspan="2"> <input type="submit" value="Donar d'alta el tripulant"/> </td>
			</tr>
		</table>
	</form:form>
</body>
</html>