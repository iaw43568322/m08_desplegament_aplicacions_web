package exercici_05_a_08.entitats;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tripulant")
public class Tripulant {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="nom")
	private String nom;
	
	@Column(name="cognom")
	private String cognom;
	
	@Column(name="email")
	private String email;
	
	@Column(name="dataCreacio")
	private LocalDateTime dataCreacio;
	
	@Column(name="departamentId")
	private int departamentId;

	/**
	 * 
	 */
	public Tripulant() {
		super();
	}

	

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}



	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}



	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}



	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}



	/**
	 * @return the cognom
	 */
	public String getCognom() {
		return cognom;
	}



	/**
	 * @param cognom the cognom to set
	 */
	public void setCognom(String cognom) {
		this.cognom = cognom;
	}



	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}



	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}



	/**
	 * @return the dataCreacio
	 */
	public LocalDateTime getDataCreacio() {
		return dataCreacio;
	}



	/**
	 * @param dataCreacio the dataCreacio to set
	 */
	public void setDataCreacio(LocalDateTime dataCreacio) {
		this.dataCreacio = dataCreacio;
	}



	/**
	 * @return the departamentId
	 */
	public int getDepartamentId() {
		return departamentId;
	}



	/**
	 * @param departamentId the departamentId to set
	 */
	public void setDepartamentId(int departamentId) {
		this.departamentId = departamentId;
	}



	@Override
	public String toString() {
		return "Tripulant [id=" + id + ", nom=" + nom + ", cognom=" + cognom + ", email=" + email + ", dataCreacio="
				+ dataCreacio + ", departamentId=" + departamentId + "]";
	}
	
	
}
