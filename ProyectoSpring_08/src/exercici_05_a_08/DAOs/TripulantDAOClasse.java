package exercici_05_a_08.DAOs;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import exercici_05_a_08.entitats.Tripulant;

@Repository
public class TripulantDAOClasse implements TripulantDAOInterface {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	@Transactional
	public List<Tripulant> getTripulants() {
		
		Session laMevaSessio = (Session) sessionFactory.getCurrentSession();
		
		Query<Tripulant> queryLlistaTripulants = laMevaSessio.createQuery("from Tripulant", Tripulant.class);
		
		List<Tripulant> llistaTripulantsDeLaBD = queryLlistaTripulants.getResultList();
		
		return llistaTripulantsDeLaBD;
	}
	
	@Override
	@Transactional
	public void insertarTripulant(Tripulant tripulantNou) {
		Session laMevaSessio = sessionFactory.getCurrentSession();
		
		laMevaSessio.save(tripulantNou);
	}

	@Override
	@Transactional
	public Tripulant getTripulant(int tripulantId) {
		// 
		Session laMevaSessio = sessionFactory.getCurrentSession();
		
		Tripulant tripulantTmp = laMevaSessio.get(Tripulant.class, tripulantId);
		
		return tripulantTmp;
	}

	@Override
	@Transactional
	public void updatejarTripulant(Tripulant tripulantPerUpdatejar) {
		// 
		Session laMevaSessio = sessionFactory.getCurrentSession();
		
		laMevaSessio.update(tripulantPerUpdatejar);
		
	}

	@Override
	@Transactional
	public void eliminarTripulant(int tripulantId) {
		// 
		Session laMevaSessio = sessionFactory.getCurrentSession();
		
		Query queryEsborrarTripulant = laMevaSessio.createQuery("DELETE from Tripulant where id = " + tripulantId);
		
		queryEsborrarTripulant.executeUpdate();
		
	}
	

}
