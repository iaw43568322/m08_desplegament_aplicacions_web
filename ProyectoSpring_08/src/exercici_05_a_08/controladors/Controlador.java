package exercici_05_a_08.controladors;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import exercici_05_a_08.DAOs.TripulantDAOInterface;
import exercici_05_a_08.entitats.Tripulant;

@Controller
@RequestMapping("/tripulant")
public class Controlador {
	
	@Autowired
	private TripulantDAOInterface tripulantDAO;
	
	@RequestMapping("/llistaTripulants")
	public String llistarTripulants(Model elModel) {
		System.out.println("Prova");
		List<Tripulant> llistaTripulantsDelDAO = tripulantDAO.getTripulants();
		
		
		elModel.addAttribute("llistaTripulants", llistaTripulantsDelDAO);
		
		return "llista_tripulants";
	}
	
	@RequestMapping("/formulariInsertTripulant")
	public String mostrarFormulariInsertTripulant(Model elModel ) {
		Tripulant nouTripulant = new Tripulant();
		
		elModel.addAttribute("elNouTripulant", nouTripulant);
		
		return "formulariInsertTripulant";
	}
	
	@PostMapping("/procesarAltaTripulant")
	public String donarDAltaTripulant(@ModelAttribute("elNouTripulant") Tripulant tripulantNou) {
		tripulantNou.setDataCreacio(LocalDateTime.now());
		tripulantDAO.insertarTripulant(tripulantNou);
		
		return "redirect:/tripulant/llistaTripulants";
	}
	
	@GetMapping("formulariUpdateTripulant")
	public String mostrarFormulariUpdateTripulant(@RequestParam("tripulantId") int tripulantId, Model elModel) {
		Tripulant tripulantTmp = tripulantDAO.getTripulant(tripulantId);
		
		elModel.addAttribute("tripulantPerUpdatejar", tripulantTmp);
		
		return "formulariUpdatejarTripulant";
	}
	
	@PostMapping("/procesarUpdateTripulant")
	public String updatejarTripulant(@ModelAttribute("tripulantPerUpdatejar") Tripulant tripulantPerUpdatejar) {
		tripulantDAO.updatejarTripulant(tripulantPerUpdatejar);
		
		return "redirect:/tripulant/llistaTripulants";
	}
	
	@GetMapping("formulariDeleteTripulant")
	public String deleteTripulant(@RequestParam("tripulantId") int tripulantId) {
		tripulantDAO.eliminarTripulant(tripulantId);
		
		return "redirect:/tripulant/llistaTripulants";
	}
}
