package curs_de_06_FormMVCTags;

public class Tripulant {
	
	private String nom;
	private String cognom;
	
	
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the cognom
	 */
	public String getCognom() {
		return cognom;
	}
	/**
	 * @param cognom the cognom to set
	 */
	public void setCognom(String cognom) {
		this.cognom = cognom;
	}
	
	
}
