package curs_de_06_FormMVCTags;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/tripulant")
public class ControladorTripulant {

	@RequestMapping("/mostrarFormulariAltaTripulant")
	public String mostrarFormulari(Model model) {
		return "formulariAltaTripulant";
	}
}
