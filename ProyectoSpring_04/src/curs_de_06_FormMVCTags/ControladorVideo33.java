package curs_de_06_FormMVCTags;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


/*
Conflicte de rutes (video 33):

Imaginem que tenim 2 controladors (FormulariWebControlador i ControladorVideo33). En tots 2 hi ha un mètode i resulta que 
els 2 mètodes tenen el mateix valor de @RequestMapping (tindran el valor /respostaDelFormulari_29).
Conflicte de rutes: ara hi ha un conflicte de rutes perquè la mateixa URL (respostaDelFormulari_29) ens porta a 2 mètodes
diferents (el del FormulariWebControlador i el del ControladorVideo33). 
En la Console sortirà un error de tipus Ambiguous mapping.
Una solució és que el @RequestMapping del mètode del controlador ControladorVideo33 tingui un altre valor, 
per exemple /respostaDelFormulari_299. Això implicarà que si volem que el <form> de formulariNom.jsp cridi al mètode
del controlador ControladorVideo33 haurem de fer <form action="respostaDelFormulari_299">.
L'altra solució és fer servir rutes relatives.

Rutes relatives:
Que cada controlador (classe de Java amb @Controller) funcioni sota una ruta relativa diferent (li posem @RequestMapping("/XXX") 
a sobre la declaració de la classe on XXX serà la ruta relativa del controlador). 
D'aquesta manera faran referència a vistes (i si volem també als mètodes que hi ha dins del controlador que seran els que 
retornin la pàgina JSP que toca) que estaran en rutes diferents i per tant es poden repetir els noms de les vistes (però no dins de cada ruta). 
D'aquesta manera podem fer que diversos controladors tinguin 1 mètode que es digui patata() que retornarà la vista patata.jsp, ja que
cada controlador tindrà les seves vistes en una ruta diferent i per tant podrem tenir tants pataja.jsp amb contingut diferent com rutes. 
Lo mateix passa amb la trucada al mètode patata() ja que el cridariem amb: ruta relativa del controlador + @RequestMapping del mètode.

La ruta relativa és com afegir un prefix a un número telefònic. El telèfon 758-54-48 d'Espanya i de Finlàndia es diferencien 
perquè porten el prefix 34 i 358 respectivament.

Amb la Java Annotation @Controller fem que la classe sigui un controlador.
Amb la Java Annotation @RequestMapping("/XXX")	fem una ruta relativa per aquest controlador. La ruta relativa serà /XXX. Ara, per
a trucar a un mètode d'aquest controlador, no podem fer-ho només amb l'identificador que té el mètode en el seu @RequestMapping, ara
li hem d'afegir la ruta relativa del controlador.

En projectes grans es molt probable que hi hagin més d'1 controlador i que cada programador tingui els seus propis controladors. 
Si es donés el cas que 2 programadors fessin servir la mateixa URL en un @RequestMapping, per evitar que hi hagi duplicitat de
URL's es faran servir les rutes relatives. Ara el @RequestMapping dels mètodes i vistes que retornen serà ruta relativa 
del controlador + @RequestMapping del mètode/vista.
 */

@Controller								// Amb aquesta java Annotation hem fet que aquesta classe sigui un controlador.
@RequestMapping("/dirSegon")			// Amb aquesta java Annotation hem fet una ruta relativa per aquest controlador.
public class ControladorVideo33 {
	// Conflicte de rutes (video 33):
	
	@RequestMapping("/respostaDelFormulari_29")
	public String procesarFormulari(HttpServletRequest request, Model model) {		// L'objecte "request" ens servirà per accedir a les dades transmeses pel formulari.
		String nomTripulant = request.getParameter("tripulantNom_33");				// El model és l'objecte que serveix per a transmetre informació entre el controlador i les vistes.
		nomTripulant = nomTripulant + " és un tripulant de la CCCP Leonov";
		
		String resposta = "ALERTA DE COL·LISIÓ DE RUTES,  " + nomTripulant;
		
		// Agregem la informació al model:
		model.addAttribute("missatgePerRetornar_33", resposta);		// Spring associa el contingut de "resposta" a "missatgePerRetornar" (per accedir al contingut de "resposta",
																	// Spring fa servir "missatgePerRetornar").
		
		return "formulariNomResposta_2";							// Aquesta serà la resposta que enviarà el sistema al rebre el formulari (la vista que farà servir).
	}
}
