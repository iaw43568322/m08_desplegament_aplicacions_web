package curs_de_01_Inicio_a_03_InyeccionesDeDependencias;

public class Maquinista implements Tripulants {
	
	public String agafarTarees() {
		return "Aquestes són les tarees del maquinista";
	}
	
	public String agafarInforme() {
		return "Informe del maquinista";
	}
}
