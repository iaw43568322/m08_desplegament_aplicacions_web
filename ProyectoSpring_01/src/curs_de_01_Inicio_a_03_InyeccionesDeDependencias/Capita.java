package curs_de_01_Inicio_a_03_InyeccionesDeDependencias;

public class Capita implements Tripulants {
	
	public String agafarTarees() {
		return "Aquestes són les tarees del capità";
	}
	
	public String agafarInforme() {
		return "Informe del capità";
	}
}
