package curs_de_01_Inicio_a_03_InyeccionesDeDependencias;

public class Electronic implements Tripulants {
	
	public String agafarTarees() {
		return "Aquestes són les tarees del electronic";
	}
	
	public String agafarInforme() {
		return "Informe de l'electronic";
	}
}
