package curs_de_01_Inicio_a_03_InyeccionesDeDependencias;

import org.springframework.context.support.*;

public class UsTripulants {

	public static void main(String[] args) {
		// Creem el contexte carregant el fitxer xml de configuració
		ClassPathXmlApplicationContext contexte = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		// Creem els objectes
		Tripulants capita = contexte.getBean("tripulantCapita", Tripulants.class);
		System.out.println(capita.agafarTarees());
		
		Tripulants maquinista = contexte.getBean("tripulantMaquinista", Tripulants.class);
		System.out.println(maquinista.agafarTarees());
		
		Tripulants electronic = contexte.getBean("tripulantElectronic", Tripulants.class);
		System.out.println(electronic.agafarTarees());

		contexte.close();
	}

}
