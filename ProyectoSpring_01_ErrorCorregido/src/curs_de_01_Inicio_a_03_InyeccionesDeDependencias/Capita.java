package curs_de_01_Inicio_a_03_InyeccionesDeDependencias;

public class Capita implements Tripulants {
	
	// Atributs
	private InformeInterface informeNou;
	
	// Constructors
	public Capita(InformeInterface inf) {
		this.informeNou = inf;
	}

	@Override
	public String agafarTarees() {
		return "Aquestes són les tarees del capità";
	}

	@Override
	public String agafarInforme() {
		return "Informe del capità: " + informeNou.getInforme();
	}

}
