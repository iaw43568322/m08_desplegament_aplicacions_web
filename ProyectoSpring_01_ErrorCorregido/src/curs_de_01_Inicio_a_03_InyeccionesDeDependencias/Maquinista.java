package curs_de_01_Inicio_a_03_InyeccionesDeDependencias;

public class Maquinista implements Tripulants {
	
	// Atributs
	private String email;
	private String nomDepartament;
	
	// GETTERS & SETTERS

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the nomDepartament
	 */
	public String getNomDepartament() {
		return nomDepartament;
	}

	/**
	 * @param nomDepartament the nomDepartament to set
	 */
	public void setNomDepartament(String nomDepartament) {
		this.nomDepartament = nomDepartament;
	}
	
	// MÈTODES

	@Override
	public String agafarTarees() {
		return "Aquestes són les tarees del maquinista";
	}

	@Override
	public String agafarInforme() {
		return "Informe del maquinista";
	}

}
