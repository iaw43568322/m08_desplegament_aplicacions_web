package curs_de_01_Inicio_a_03_InyeccionesDeDependencias;

public class Electronic implements Tripulants {
	
	// Atributs
	private InformeInterface informeNou;
	private String email;
	private String nomDepartament;
	
	// Getters & Setters

	public InformeInterface getInformeNou() {
		return informeNou;
	}

	public void setInformeNou(InformeInterface informeNou) {
		this.informeNou = informeNou;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNomDepartament() {
		return nomDepartament;
	}

	public void setNomDepartament(String nomDepartament) {
		this.nomDepartament = nomDepartament;
	}

	@Override
	public String agafarTarees() {
		return "Aquestes són les tarees del electronic";
	}

	@Override
	public String agafarInforme() {
		return "Informe de l'electronic: " + informeNou.getInforme();
	}

}
