package curs_de_01_Inicio_a_03_InyeccionesDeDependencias;

import org.springframework.context.support.*;

public class UsTripulants {

	public static void main(String[] args) {
		// Creem el contexte carregant el fitxer xml de configuració
		ClassPathXmlApplicationContext contexte = new ClassPathXmlApplicationContext("applicationContext.xml");

		// Creem els objectes
		Tripulants capita = contexte.getBean("tripulantCapita", Tripulants.class);
		System.out.println(capita.agafarTarees() + "\n" + capita.agafarInforme());

		Tripulants maquinista = contexte.getBean("tripulantMaquinista", Tripulants.class);
		System.out.println(maquinista.agafarTarees());

		Maquinista maquinista2 = contexte.getBean("tripulantMaquinista", Maquinista.class);
		System.out.println(
				"Email maquinista: " + maquinista2.getEmail() + "\nDepartament: " + maquinista2.getNomDepartament());

		Tripulants electronic = contexte.getBean("tripulantElectronic", Tripulants.class);
		System.out.println(electronic.agafarTarees() + "\n" + electronic.agafarInforme());

		Electronic electronic2 = contexte.getBean("tripulantElectronic", Electronic.class);
		System.out.println("Email de l'electrònic: " + electronic2.getEmail() + "\nDepartament: "
				+ electronic2.getNomDepartament());

		// Tanquem el contexte
		contexte.close();

	}

}
