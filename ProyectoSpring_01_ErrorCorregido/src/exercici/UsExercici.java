package exercici;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsExercici {

	public static void main(String[] args) {
		
		ClassPathXmlApplicationContext contexte = new ClassPathXmlApplicationContext("applicationContext_Exercici.xml");
		
		System.out.println("--------Exercici 1--------");
		// Petició de beans al contenidor Spring
		Ruta rutaSingleton_1 = contexte.getBean("rutaSingleton", Ruta.class);
		Ruta rutaSingleton_2 = contexte.getBean("rutaSingleton", Ruta.class);
		
		System.out.println("Direcció memòria rutaSingleton_1: " + rutaSingleton_1);
		System.out.println("Direcció memòria rutaSingleton_2: " + rutaSingleton_2);
		System.out.println("--------");
		System.out.println("rutaSingleton_1.getId(): " + rutaSingleton_1.getId());
		System.out.println("rutaSingleton_1.getNom(): " + rutaSingleton_1.getNom());
		System.out.println("rutaSingleton_1.getWaypoint(): " + rutaSingleton_1.getWaypoint());
		System.out.println("rutaSingleton_1.getLlistaWaypoints(): " + rutaSingleton_1.getLlistaWaypoints());
		System.out.println("rutaSingleton_1.getWaypoint().getId(): " + rutaSingleton_1.getWaypoint().getId());
		System.out.println("rutaSingleton_1.getWaypoint().getNom(): " + rutaSingleton_1.getWaypoint().getNom());
		
		contexte.close();
	}

}
