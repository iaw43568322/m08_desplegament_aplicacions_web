package exercici;

import java.time.LocalDate;
import java.util.ArrayList;


public class Ruta {
	
	// ATRIBUTS
	private int id;
	private String nom;
	private Waypoint waypoint;
	private ArrayList<Waypoint> llistaWaypoints;
	private LocalDate dataCreacio;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the waypoint
	 */
	public Waypoint getWaypoint() {
		return waypoint;
	}
	/**
	 * @param waypoint the waypoint to set
	 */
	public void setWaypoint(Waypoint waypoint) {
		this.waypoint = waypoint;
	}
	/**
	 * @return the llistaWaypoints
	 */
	public ArrayList<Waypoint> getLlistaWaypoints() {
		return llistaWaypoints;
	}
	/**
	 * @param llistaWaypoints the llistaWaypoints to set
	 */
	public void setLlistaWaypoints(ArrayList<Waypoint> llistaWaypoints) {
		this.llistaWaypoints = llistaWaypoints;
	}
	/**
	 * @return the dataCreacio
	 */
	public LocalDate getDataCreacio() {
		return dataCreacio;
	}
	/**
	 * @param dataCreacio the dataCreacio to set
	 */
	public void setDataCreacio(LocalDate dataCreacio) {
		this.dataCreacio = dataCreacio;
	}

}
