package exercici;

import java.time.LocalDate;

public class Waypoint {
	
	// ATRIBUTS
	private int id;
	private String nom;
	private LocalDate dataCreacio;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the dataCreacio
	 */
	public LocalDate getDataCreacio() {
		return dataCreacio;
	}
	/**
	 * @param dataCreacio the dataCreacio to set
	 */
	public void setDataCreacio(LocalDate dataCreacio) {
		this.dataCreacio = dataCreacio;
	}

}
