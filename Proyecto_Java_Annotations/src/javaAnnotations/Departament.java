package javaAnnotations;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("dep-B")
public class Departament {

	// Atributs
	@Value("${iddept}")
	public int ID;
	@Value("${nomdept}")
	public String nom;
	@Value("${emaildept}")
	public String email;
	public Organitzacio organitzacio;
	public Dep_B_per_1 capDepartament;
	private final BeanFactory factory;
	public List<Dep_B_per_2> llistaPersonal;
	public InformeDepartament informe;
	
	
	/**
	 * @param factory
	 */
	@Autowired
	public Departament(BeanFactory factory) {
		super();
		this.factory = factory;
	}


	/**
	 * @return the iD
	 */
	public int getID() {
		return ID;
	}


	/**
	 * @param iD the iD to set
	 */
	public void setID(int iD) {
		ID = iD;
	}


	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}


	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}


	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}


	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}


	/**
	 * @return the organitzacio
	 */
	public Organitzacio getOrganitzacio() {
		return organitzacio;
	}


	/**
	 * @param organitzacio the organitzacio to set
	 */
	@Autowired
	public void setOrganitzacio(Organitzacio organitzacio) {
		this.organitzacio = organitzacio;
	}


	/**
	 * @return the capDepartament
	 */
	public Dep_B_per_1 getCapDepartament() {
		return capDepartament;
	}


	/**
	 * @param capDepartament the capDepartament to set
	 */
	public void setCapDepartament(Dep_B_per_1 capDepartament) {
		this.capDepartament = capDepartament;
	}


	/**
	 * @return the llistaPersonal
	 */
	public List<Dep_B_per_2> getLlistaPersonal() {
		return llistaPersonal;
	}


	/**
	 * @param llistaPersonal the llistaPersonal to set
	 */
	public void setLlistaPersonal(List<Dep_B_per_2> llistaPersonal) {
		this.llistaPersonal = llistaPersonal;
	}


	/**
	 * @return the informe
	 */
	public InformeDepartament getInforme() {
		return informe;
	}


	/**
	 * @param informe the informe to set
	 */
	public void setInforme(InformeDepartament informe) {
		this.informe = informe;
	}
	
	public void addPersones(List<Dep_B_per_2> someArrayList) {
		this.llistaPersonal = someArrayList.stream().map(param -> factory.getBean(Dep_B_per_2.class, param)).collect(Collectors.toList());
	}


	@Override
	public String toString() {
		return "Departament [ID=" + ID + ", nom=" + nom + ", email=" + email + ", capDepartament=" + capDepartament
				+ ", llistaPersonal=" + llistaPersonal + "]";
	}
	
	
	
}
