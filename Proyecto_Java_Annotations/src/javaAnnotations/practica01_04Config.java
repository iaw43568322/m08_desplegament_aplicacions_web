package javaAnnotations;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan("annotations")
@PropertySource("classpath:practica01-04Departaments.propietats")
public class practica01_04Config {

	// Creem els Bean del Informe i de la Organització
	@Bean
	public Informe informe() {
		return new Informe();
	}
	
	@Bean
	public Organitzacio organitzacio() {
		return new Organitzacio(informe());
	}
}
