package javaAnnotations;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class usPractica01_04Exercici2 {

	public static void main(String[] args) {
		AnnotationConfigApplicationContext contexte = new AnnotationConfigApplicationContext(practica01_04Config.class);
		
		Departament dep1 = contexte.getBean("dep-B", Departament.class);
		
		System.out.println("departament 1: " + dep1);
		
		PersonalInterface pi = contexte.getBean("capDepartament", Dep_B_per_1.class);
		
		Organitzacio org1 = contexte.getBean("organitzacio", Organitzacio.class);
		
		System.out.println("Organitzacio @Bean .toString() " + org1);
		System.out.println("Contingut informe organització per bean (org1.informe.textAImprimirPerPantalla()) " + 
								org1.informe.textAImprimirPerPantalla());

	}

}
