package javaAnnotations;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class Dep_B_per_2 implements PersonalInterface {

	// Declaració de variables
	public int id;
	public String nom;
	
	/**
	 * @param id
	 * @param nom
	 */
	protected Dep_B_per_2(int id, String nom) {
		super();
		this.id = id;
		this.nom = nom;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	@Override
	public String toString() {
		return "Dep_B_per_2 [id=" + id + ", nom=" + nom + "]";
	}
	
	
	
}
