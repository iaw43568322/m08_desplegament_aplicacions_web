package javaAnnotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Organitzacio {

	// Atributs
	public static final String ORG_CODE = "orga";
	public static int orgNum = 0;
	public String nom;
	public Informe informe;
	
	/**
	 * @param informe
	 */
	@Autowired
	public Organitzacio(Informe informe) {
		super();
		this.informe = informe;
	}

	@Override
	public String toString() {
		return "Organitzacio [nom=" + nom + ", informe=" + informe + "]";
	}
}
