/**
 * 
 */
package practica01_04;

import java.util.ArrayList;

/**
 * @author ignasibrugada
 *
 */

public class Departament {
	// Atributs
	private int ID;
	private String nom;
	private String email;
	private Organitzacio organitzacio;
	private Personal capDeDepartament;
	private ArrayList<Personal> llistaPersonal;
	// private informeDepartament informe;
	
	/**
	 * @param iD
	 * @param nom
	 * @param email
	 * @param organitzacio
	 * @param capDeDepartament
	 * @param llistaPersonal
	 * @param informe
	 */
	public Departament(int iD, String nom, String email, Organitzacio organitzacio, Personal capDeDepartament,
			ArrayList<Personal> llistaPersonal) {
		super();
		ID = iD;
		this.nom = nom;
		this.email = email;
		this.organitzacio = organitzacio;
		this.capDeDepartament = capDeDepartament;
		this.llistaPersonal = llistaPersonal;
		// this.informe = informe;
	}

	/**
	 * @return the iD
	 */
	public int getID() {
		return ID;
	}

	/**
	 * @param iD the iD to set
	 */
	public void setID(int iD) {
		ID = iD;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the organitzacio
	 */
	public Organitzacio getOrganitzacio() {
		return organitzacio;
	}

	/**
	 * @param organitzacio the organitzacio to set
	 */
	public void setOrganitzacio(Organitzacio organitzacio) {
		this.organitzacio = organitzacio;
	}

	/**
	 * @return the capDeDepartament
	 */
	public Personal getCapDeDepartament() {
		return capDeDepartament;
	}

	/**
	 * @param capDeDepartament the capDeDepartament to set
	 */
	public void setCapDeDepartament(Personal capDeDepartament) {
		this.capDeDepartament = capDeDepartament;
	}

	/**
	 * @return the llistaPersonal
	 */
	public ArrayList<Personal> getLlistaPersonal() {
		return llistaPersonal;
	}

	/**
	 * @param llistaPersonal the llistaPersonal to set
	 */
	public void setLlistaPersonal(ArrayList<Personal> llistaPersonal) {
		this.llistaPersonal = llistaPersonal;
	}

	/**
	 * @return the informe
	 */
	/*
	public informeDepartament getInforme() {
		return informe;
	}
	*/
	/**
	 * @param informe the informe to set
	 */
	/*
	public void setInforme(informeDepartament informe) {
		this.informe = informe;
	}
	*/
	
	public void mostrarEmpleados() {
		for (Personal personal : llistaPersonal) {
			System.out.println(personal);
		}
	}	
}
