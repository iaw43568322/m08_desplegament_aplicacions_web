package practica01_04;

public class Informe {
	// Atributs
	private String textAImprimirPerPantalla;
	
	/**
	 * @param textAImprimirPerPantalla
	 */
	public Informe(String textAImprimirPerPantalla) {
		super();
		this.textAImprimirPerPantalla = textAImprimirPerPantalla;
	}

	/**
	 * @return the textAImprimirPerPantalla
	 */
	public String getTextAImprimirPerPantalla() {
		return textAImprimirPerPantalla;
	}

	/**
	 * @param textAImprimirPerPantalla the textAImprimirPerPantalla to set
	 */
	public void setTextAImprimirPerPantalla(String textAImprimirPerPantalla) {
		this.textAImprimirPerPantalla = textAImprimirPerPantalla;
	}

	

}
