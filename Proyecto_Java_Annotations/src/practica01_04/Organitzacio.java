package practica01_04;

public class Organitzacio {
	
	// Atributs
	private String nom;
	private Informe informe;
	
	
	public Organitzacio(String nom, Informe informe) {
		super();
		this.nom = nom;
		this.informe = informe;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public Informe getInforme() {
		return informe;
	}


	public void setInforme(Informe informe) {
		this.informe = informe;
	}

}
