package practica01_04;

import org.springframework.context.support.*;

public class usPractica01_04 {

	public static void main(String[] args) {
		// 
		
		System.out.println("-------Fichero XML-----");
		
		ClassPathXmlApplicationContext contexte = new ClassPathXmlApplicationContext("applicationContext_Practica01-04.xml");
		
		Departament dep_A =  contexte.getBean("departamentTipus1", Departament.class);
		
		System.out.println(dep_A.getNom());
		dep_A.mostrarEmpleados();	
		System.out.println(dep_A.getCapDeDepartament());
		contexte.close();
		}
}
