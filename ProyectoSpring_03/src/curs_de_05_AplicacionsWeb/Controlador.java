package curs_de_05_AplicacionsWeb;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Controlador {
	/**
	 * Mètode que s'encarrega d'especificar quina vista
	 * volem fer servir
	 */
	@RequestMapping
	public String mostrarVista1() {
		return "vistaExemple_1.jsp";
	}
}
