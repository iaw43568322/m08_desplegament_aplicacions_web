package practica01_04JA;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("departamentTipus2")
public class Departament {
	@Value("${id}")
	private int ID;
	@Value("${nom}")
	private String nom;
	@Value("${email}")
	private String email;
	private Organitzacio organitzacio;
	private PersonalInterface capDeDepartament;	
	@Autowired
	private List<dep_B_per_2> llistaPersonal;
	//private informeDepartament informe;
	
	
	/**
	 * @return the organitzacio
	 */
	public Organitzacio getOrganitzacio() {
		return organitzacio;
	}
	/**
	 * @param organitzacio the organitzacio to set
	 */
	@Autowired
	public void setOrganitzacio(Organitzacio organitzacio) {
		this.organitzacio = organitzacio;
	}

	/**
	 * @return the capDeDepartament
	 */
	public PersonalInterface getCapDeDepartament() {
		return capDeDepartament;
	}
	/**
	 * @param capDeDepartament the capDeDepartament to set
	 */
	@Autowired
	public void setCapDeDepartament(dep_B_per_1 capDeDepartament) {
		this.capDeDepartament = capDeDepartament;
	}
	
	/**
	 * @return the llistaPersonal
	 */
	public List<dep_B_per_2> getLlistaPersonal() {
		return llistaPersonal;
	}	


}
