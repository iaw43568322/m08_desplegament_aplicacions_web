package practica01_04JA;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class usPractica01_04 {

	public static void main(String[] args) {			
		
		
		System.out.println("\n-------Java Annotations-----");
		AnnotationConfigApplicationContext contexte1 = new AnnotationConfigApplicationContext(Practica01_04Config.class);
		Departament dep_B = contexte1.getBean("departamentTipus2", Departament.class);
		System.out.println(dep_B);
		System.out.println(dep_B.getOrganitzacio());
		System.out.println(dep_B.getCapDeDepartament());
		for (dep_B_per_2 a : dep_B.getLlistaPersonal()) {
			System.out.println(a);
		}			
	
		dep_B_per_2 p3 = contexte1.getBean("dep_B_per_2", dep_B_per_2.class);
		System.out.println(p3);
		contexte1.close();
	}
}
