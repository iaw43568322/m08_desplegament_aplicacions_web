package practica01_04;

import org.springframework.context.support.ClassPathXmlApplicationContext;


public class usPractica01_04 {

	public static void main(String[] args) {
		
		System.out.println("-------Fichero XML-----");
		//Cargamos contexte
		ClassPathXmlApplicationContext contexte = new ClassPathXmlApplicationContext("applicationContext_Practica01-04.xml");
		//Creamos dep_A
		Departament dep_A =  contexte.getBean("departamentTipus1", Departament.class);
		//Imprimimos datos para comprobar que se han creado correctamente
		System.out.println(dep_A.getNom());
		dep_A.mostrarEmpleados();	
		System.out.println(dep_A.getCapDeDepartament());
		contexte.close();
		}
}
