package exercici_05_a_07;

import java.util.ArrayList;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


@Component				// Java Annotation perquè es crei un bean d'aquesta classe automàticament.
@Scope("prototype")
public class Departament {
	private int id;
	private String nom;
	private String email;
	private Organitzacio organitzacio;
	private Tripulant capDeDepartament;
	private ArrayList<Tripulant> llistaTripulants;
	
	
	// Aquest constructor el fa servir el ServiceClassMultiparametres2_v2.
	public Departament(int id) {
		this.id = id;
	}

	// Aquest constructor el fa servir el ServiceClassMultiparametres2_v2.
	public Departament(int id, String nom, String email) {
		this.id = id;
		this.nom = nom;
		this.email = email;
	}
	

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Organitzacio getOrganitzacio() {
		return organitzacio;
	}
	public void setOrganitzacio(Organitzacio organitzacio) {
		this.organitzacio = organitzacio;
	}
	public Tripulant getCapDeDepartament() {
		return capDeDepartament;
	}
	public void setCapDeDepartament(Tripulant capDeDepartament) {
		this.capDeDepartament = capDeDepartament;
	}
	public ArrayList<Tripulant> getLlistaTripulants() {
		return llistaTripulants;
	}
	public void setLlistaTripulants(ArrayList<Tripulant> llistaTripulants) {
		this.llistaTripulants = llistaTripulants;
	}

	
	@Override
	public String toString() {
		return "Departament [id=" + id + ", nom=" + nom + ", email=" + email + "]";
	}

	
	
}
