package exercici_05_a_07;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;





public class usAplicacio {
	
	static public List<Tripulant> inicialitzarTripulants() {
		AnnotationConfigApplicationContext contexteAmbClasseConfig = new AnnotationConfigApplicationContext(aplicacioConfig.class);
		
		// Exemple d'inserció obj's <> inicialitzats amb 3 paràmetres:
		// https://stackoverflow.com/questions/55096855/how-to-ask-for-prototype-bean-in-spring-service-class-without-applicationcontext
		// Al final de todo, la respuesta del 11 de marzo del 2019 de Andy Brown.
		
		ServiceClassMultiparametres serviceClassMultiparametres = contexteAmbClasseConfig.getBean("serviceClassMultiparametres", ServiceClassMultiparametres.class);
		
		ArrayList<ObjIniProtoTypeBeanMultiparametres> llistaObjIniProtoTypeBeanMultiparametres = new ArrayList();
		ObjIniProtoTypeBeanMultiparametres tripulant_1 = new ObjIniProtoTypeBeanMultiparametres("10", "tripulant_10", LocalDate.now(), 100);
		ObjIniProtoTypeBeanMultiparametres tripulant_2 = new ObjIniProtoTypeBeanMultiparametres("20", "tripulant_20", LocalDate.now(), 200);
		ObjIniProtoTypeBeanMultiparametres tripulant_3 = new ObjIniProtoTypeBeanMultiparametres("30", "tripulant_30", LocalDate.now(), 100);
		llistaObjIniProtoTypeBeanMultiparametres.add(tripulant_1);
		llistaObjIniProtoTypeBeanMultiparametres.add(tripulant_2);
		llistaObjIniProtoTypeBeanMultiparametres.add(tripulant_3);
		
		serviceClassMultiparametres.demoMethod(llistaObjIniProtoTypeBeanMultiparametres);
		
		contexteAmbClasseConfig.close();
		
		return serviceClassMultiparametres.getTripulantMultiparametres();
	}
	
	

	
	static public List<Departament> inicialitzarDepartaments_v2() {
		AnnotationConfigApplicationContext contexteAmbClasseConfig = new AnnotationConfigApplicationContext(aplicacioConfig.class);
		
		// Exemple d'inserció obj's <> inicialitzats amb 3 paràmetres (versió 2):
		
		ServiceClassMultiparametres2_v2 serviceClassMultiparametres2_v2 = contexteAmbClasseConfig.getBean("serviceClassMultiparametres2_v2", ServiceClassMultiparametres2_v2.class);
		
		List<Integer> llistaIdsDepartament = Arrays.asList(102, 202, 302, 402);
		List<String> llistaNomsDepartament = Arrays.asList("departament_102", "departament_202", "departament_302", "departament_402");
		List<String> llistaEmailsDepartament = Arrays.asList("departament_102@gmail.cat", "departament_202@gmail.cat", "departament_302@gmail.cat", "departament_402@gmail.cat");
		
		serviceClassMultiparametres2_v2.demoMethod_v2(llistaIdsDepartament, llistaNomsDepartament, llistaEmailsDepartament);
		
		contexteAmbClasseConfig.close();
		
		return serviceClassMultiparametres2_v2.getLlistaDepartamentsMultiparametres();
	}
	
	
	
	static public List<Departament> inicialitzarDepartaments_v3() {
		AnnotationConfigApplicationContext contexteAmbClasseConfig = new AnnotationConfigApplicationContext(aplicacioConfig.class);
		
		// Exemple d'inserció obj's <> inicialitzats amb 3 paràmetres (versió 3):
		
		ServiceClassMultiparametres2_v3 serviceClassMultiparametres2_v3 = contexteAmbClasseConfig.getBean("serviceClassMultiparametres2_v3", ServiceClassMultiparametres2_v3.class);
		
		// La llista 'llistaPosicionsEnArray' conté les posicions que es fan servir en les llistes, hi haurà 4 departaments i --> 0 fins a 3.		
		List<Integer> llistaPosicionsEnArray = Arrays.asList(0, 1, 2, 3);
		List<Integer> llistaIdsDepartament = Arrays.asList(301, 302, 303, 304);
		List<String> llistaNomsDepartament = Arrays.asList("departament_301", "departament_302", "departament_303", "departament_304");
		List<String> llistaEmailsDepartament = Arrays.asList("departament_301@gmail.cat", "departament_302@gmail.cat", "departament_303@gmail.cat", "departament_304@gmail.cat");
		
		serviceClassMultiparametres2_v3.demoMethod_v3(llistaPosicionsEnArray, llistaIdsDepartament, llistaNomsDepartament, llistaEmailsDepartament);
		
		contexteAmbClasseConfig.close();
		
		return serviceClassMultiparametres2_v3.getLlistaDepartamentsMultiparametres();
	}

}
