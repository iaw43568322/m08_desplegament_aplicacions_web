package exercici_05_a_07;

import java.time.LocalDate;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class Controlador {
	List<Tripulant> llistaProtoTypeBeanMultiparametresTripulants;
	List<Departament> llistaProtoTypeBeanMultiparametresDepartaments_v2;
	List<Departament> llistaProtoTypeBeanMultiparametresDepartaments_v3;
	
	
	@RequestMapping("/")
	public String inici() {
		llistaProtoTypeBeanMultiparametresTripulants = usAplicacio.inicialitzarTripulants();
		llistaProtoTypeBeanMultiparametresDepartaments_v2 = usAplicacio.inicialitzarDepartaments_v2();
		llistaProtoTypeBeanMultiparametresDepartaments_v3 = usAplicacio.inicialitzarDepartaments_v3();
		
		System.out.println();
		System.out.println("------llistaProtoTypeBeanMultiparametresTripulants------");
		
		int j = 1;
		for(Tripulant tripulantTmp : llistaProtoTypeBeanMultiparametresTripulants) {
			System.out.println(j + ": TRIPULANT = " + tripulantTmp + 
					"\n      tripulantTmp.getDni() = " + tripulantTmp.getDni() +
					"\n      tripulantTmp.getNom() = " + tripulantTmp.getNom() +
					"\n      tripulantTmp.getDepartament() = " + tripulantTmp.getDepartament() +
					"\n      tripulantTmp.getDataCreacio() = " + tripulantTmp.getDataCreacio());
			j++;
		}
		
		System.out.println();
		System.out.println("------llistaProtoTypeBeanMultiparametresDepartaments_v2------");
		
		j = 1;
		for(Departament departamentTmp_v2 : llistaProtoTypeBeanMultiparametresDepartaments_v2) {
			System.out.println(j + ": DEPARTAMENT_v2" + 
					"\n      departamentTmp_v2.getId() = " + departamentTmp_v2.getId() +
					"\n      departamentTmp_v2.getNom() = " + departamentTmp_v2.getNom() + 
					"\n      departamentTmp_v2.getNom() = " + departamentTmp_v2.getEmail());
			j++;
		}
		
		System.out.println();
		System.out.println("------llistaProtoTypeBeanMultiparametresDepartaments_v3------");
		j = 1;
		for(Departament departamentTmp_v3 : llistaProtoTypeBeanMultiparametresDepartaments_v3) {
			System.out.println(j + ": DEPARTAMENT_v3" + 
					"\n      departamentTmp_v3.getId() = " + departamentTmp_v3.getId() +
					"\n      departamentTmp_v3.getNom() = " + departamentTmp_v3.getNom() + 
					"\n      departamentTmp_v3.getNom() = " + departamentTmp_v3.getEmail());
			j++;
		}
		
		return "inici";
	}
	
	
}
