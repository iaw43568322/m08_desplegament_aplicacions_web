
CREATE TABLE `nau` (  `id` int(3) NOT NULL AUTO_INCREMENT,  `nom` varchar(128) DEFAULT NULL,  PRIMARY KEY (`id`)  ) ENGINE=INNODB;

CREATE TABLE `departament` (  `id` int(3) NOT NULL AUTO_INCREMENT,  `nom` varchar(128) DEFAULT NULL,  `email` varchar(100) DEFAULT NULL,  `nauId` int(3) DEFAULT NULL,  `capDeDepartament` int(3) DEFAULT NULL,  PRIMARY KEY (`id`),  KEY `FK_NAU_ID` (`nauId`)) ENGINE=INNODB;

CREATE TABLE `tripulant` (  `id` int(3) NOT NULL AUTO_INCREMENT,  `nom` varchar(128) DEFAULT NULL,  `cognom` varchar(128) DEFAULT NULL,  `login` varchar(128) DEFAULT NULL,  `departamentId` int(3) DEFAULT NULL,  `coneixements` text DEFAULT NULL,  `ciutatNaixement` varchar(128) DEFAULT NULL,  `idiomes` text DEFAULT NULL,  `edat` int(3) DEFAULT NULL,  `email` varchar(100) DEFAULT NULL,  `telefon` varchar(12) DEFAULT NULL,  `dataCreacio` date DEFAULT NULL,  PRIMARY KEY (`id`)) ENGINE=INNODB;

INSERT INTO nau (nom) VALUES("Rotarran");

INSERT     INTO     `departament`(`nom`,     `email`,     `nauId`,     `capDeDepartament`)     VALUES("Navegació", "navegacio@rotarran.ik", 1, 1);

INSERT    INTO   `tripulant`(`nom`,    `cognom`,   `login`,   `departamentId`,   `coneixements`,`ciutatNaixement`,   `idiomes`,   `edat`,   `email`,   `telefon`,   `dataCreacio`)   VALUES   ("Worf","Pagh",   "worfPagh",   1,   "astronomia   i   navegació",   "Cronos",   "klingon",   35,   "worf@ratorran.ik",948751258915,"2021-01-23");

INSERT    INTO   `tripulant`(`nom`,    `cognom`,   `login`,   `departamentId`,   `coneixements`,`ciutatNaixement`,   `idiomes`,   `edat`,   `email`,   `telefon`,   `dataCreacio`)   VALUES   ("Kurn","Pagh",   "KurnPagh",   1,   "Astronomia",   "Cronos",   "klingon",   58,   "kurnpagh@imperiklingon.ik",984587562158, "2021-01-21");

ALTER   TABLE   `departament`   ADD   CONSTRAINT   `FK_NAU_ID`   FOREIGN   KEY   (`nauId`)REFERENCES `nau` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `departament` ADD CONSTRAINT `FK_CAP_DE_DEPARTAMENT` FOREIGN KEY(`capDeDepartament`) REFERENCES `tripulant` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER   TABLE   `tripulant`   ADD   CONSTRAINT   `FK_DEPARTAMENT_ID`   FOREIGN   KEY(`departamentId`) REFERENCES `departament` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
