package curs_de_08_Acceso_a_datos;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HQLUpdatePlanetes {

	public static void main(String[] args) {
		// 
		SessionFactory elMeuFactory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Planetes.class).buildSessionFactory();
		Session elMeuSession = elMeuFactory.openSession();
		
		try {
			System.out.println("Iniciem la transacció");
			elMeuSession.beginTransaction();
			
			/* 1a forma de fer UPDATE */
			
			System.out.println("Primera forma de fer UPDATE\n");
			// Agafem de la base de dades el planeta amb id 32
			int planetaId = 3;
			Planetes planetaTmp = elMeuSession.get(Planetes.class, planetaId);
			
			// Modifiquem el valor d'un atribut a través del setter
			planetaTmp.setNom("Venus");
			
			System.out.println();
			
			/* 2a forma de fer UPDATE */
			
			System.out.println("Segona forma de fer UPDATE\n");
			// Creem la sentència HQL
			String sentenciaHQL = "UPDATE Planetes p SET p.nom='Jupiter' WHERE p.descripcio LIKE '%roig%'";
			// Llançem la sentència
			elMeuSession.createQuery(sentenciaHQL).executeUpdate();
			
			elMeuSession.getTransaction().commit();
			
		} finally {
			elMeuSession.close();
			elMeuFactory.close();
		}

	}

}
