package curs_de_08_Acceso_a_datos;

import java.time.LocalDate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class GuardarEnBDPlanetes {

	public static void main(String[] args) {
		// 
		SessionFactory elMeuFactory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Planetes.class).buildSessionFactory();
		Session elMeuSession = elMeuFactory.openSession();
		Planetes planeta_1 = new Planetes("Mart", "Planeta roig", LocalDate.of(1610,  12, 31));
		Planetes planeta_2 = new Planetes("Saturn", "Planeta gaseós", LocalDate.of(1610,  1, 18));
		
		try {
			System.out.println("Iniciem la transacció");
			elMeuSession.beginTransaction();
			
			elMeuSession.save(planeta_1);
			
			elMeuSession.flush();
			elMeuSession.clear();
			
			elMeuSession.save(planeta_2);
			
			elMeuSession.getTransaction().commit();
			
			System.out.println("Acabem la transacció de Mart");
			System.out.println("Acabem la transacció de Saturn");
			
			elMeuSession.beginTransaction();
			
			System.out.println("Lectura del registre amb id = " + planeta_1.getId());
			
			Planetes planetaLlegitDeLaBD = elMeuSession.get(Planetes.class, planeta_1.getId());
			
			System.out.println("Registre llegit de la BD: " + planetaLlegitDeLaBD);
			
			elMeuSession.getTransaction().commit();
			
			System.out.println("Hem acabat de llegir un registre de la BD");
		} finally {
			System.out.println("Tanquem elMeuSession i elMeuFactory");
			elMeuSession.close();
			elMeuFactory.close();
		}

	}

}
