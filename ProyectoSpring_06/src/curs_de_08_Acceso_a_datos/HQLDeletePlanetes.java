package curs_de_08_Acceso_a_datos;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HQLDeletePlanetes {

	public static void main(String[] args) {
		// 
		SessionFactory elMeuFactory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Planetes.class).buildSessionFactory();
		Session elMeuSession = elMeuFactory.openSession();
		
		try {
			System.out.println("Iniciem la transacció");
			elMeuSession.beginTransaction();
			
			
			// Creem la sentència HQL
			String sentenciaHQL = "DELETE Planetes p WHERE p.nom='Jupiter'";
			// Llançem la sentència
			elMeuSession.createQuery(sentenciaHQL).executeUpdate();
			
			elMeuSession.getTransaction().commit();
			
		} finally {
			elMeuSession.close();
			elMeuFactory.close();
		}

	}

}
