package curs_de_08_Acceso_a_datos;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HQLSelectPlanetes {

	public static void main(String[] args) {
		// 
		SessionFactory elMeuFactory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Planetes.class).buildSessionFactory();
		Session elMeuSession = elMeuFactory.openSession();
		
		try {
			System.out.println("Iniciem la transacció");
			elMeuSession.beginTransaction();
			
			// Creem la consulta
			// String sentenciaHQL = "FROM Planetes";
			// String sentenciaHQL = "FROM Planetes p WHERE p.nom = 'Mart'";
			String sentenciaHQL = "FROM Planetes p WHERE p.nom = 'Mart' OR p.descripcio LIKE '%gaseós%'";
			List<Planetes> llistaPlanetes = elMeuSession.createQuery(sentenciaHQL).getResultList();
			
			// Recorrem la llista per mostrar els resultats
			for (Planetes planetaTmp : llistaPlanetes) {
				System.out.println(planetaTmp);
			}
			
		} finally {
			elMeuSession.close();
			elMeuFactory.close();
		}
	}

}
