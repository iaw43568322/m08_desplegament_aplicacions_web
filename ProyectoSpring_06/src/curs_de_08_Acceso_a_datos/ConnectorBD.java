package curs_de_08_Acceso_a_datos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectorBD {

	public static void main(String[] args) {
		
		String servidorBDUrl = "jdbc:mysql://localhost:3306/LeonovBD?useSSL=false";
		String usuari = "root";
		String contrasenya = "2965bruan";
		
		try {
			Connection conexio = DriverManager.getConnection(servidorBDUrl, usuari, contrasenya);
			
			System.out.println("Connexió amb èxit");
		} catch (SQLException e) {
			System.out.println("e.getMessage(): " + e.getMessage());
			e.printStackTrace();
		}

	}

}
