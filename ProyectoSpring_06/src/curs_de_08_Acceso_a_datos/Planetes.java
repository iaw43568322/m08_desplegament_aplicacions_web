package curs_de_08_Acceso_a_datos;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="planetes")
public class Planetes {
	// Atributs
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="nom")
	private String nom;
	
	@Column(name="descripcio")
	private String descripcio;
	
	@Column(name="dataDescubriment")
	private LocalDate dataDescubriment;

	/**
	 * 
	 */
	public Planetes() {
		super();
	}

	/**
	 * @param nom
	 * @param descripcio
	 * @param dataDescubriment
	 */
	public Planetes(String nom, String descripcio, LocalDate dataDescubriment) {
		super();
		this.nom = nom;
		this.descripcio = descripcio;
		this.dataDescubriment = dataDescubriment;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the descripcio
	 */
	public String getDescripcio() {
		return descripcio;
	}

	/**
	 * @param descripcio the descripcio to set
	 */
	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}

	/**
	 * @return the daraDescrubiment
	 */
	public LocalDate getDataDescubriment() {
		return dataDescubriment;
	}

	/**
	 * @param daraDescrubiment the daraDescrubiment to set
	 */
	public void setDataDescrubiment(LocalDate dataDescubriment) {
		this.dataDescubriment = dataDescubriment;
	}

	@Override
	public String toString() {
		return "Planetes [id=" + id + ", nom=" + nom + ", descripcio=" + descripcio + ", dataDescubriment="
				+ dataDescubriment + "]";
	}
}
