package exercici_05_a_07;

import java.util.ArrayList;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component //(SpringConstants.COMPONENT_WITH_PARAMS)				// Java Annotation perquè es crei un bean d'aquesta classe automàticament.
@Scope("prototype")

public class Departament {
	
	// ATRIBUTS
	private int id;
	private String nom;
	private String email;
	private Organitzacio organitzacio;
	private Usuari capDeDepartament;
	private ArrayList<Usuari> llistaPersonal;
	
	// CONSTRUCTORS
	
	/**
	 * @param id
	 */
	public Departament(int id) {
		super();
		this.id = id;
	}

	/**
	 * @param id
	 * @param nom
	 * @param email
	 * @param organitzacio
	 * @param capDeDepartament
	 * @param llistaPersonal
	 */
	public Departament(int id, String nom, String email, Organitzacio organitzacio, Usuari capDeDepartament,
			ArrayList<Usuari> llistaPersonal) {
		super();
		this.id = id;
		this.nom = nom;
		this.email = email;
		this.organitzacio = organitzacio;
		this.capDeDepartament = capDeDepartament;
		this.llistaPersonal = llistaPersonal;
	}
	
	// GETTERS & SETTERS

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the organitzacio
	 */
	public Organitzacio getOrganitzacio() {
		return organitzacio;
	}

	/**
	 * @param organitzacio the organitzacio to set
	 */
	public void setOrganitzacio(Organitzacio organitzacio) {
		this.organitzacio = organitzacio;
	}

	/**
	 * @return the capDeDepartament
	 */
	public Usuari getCapDeDepartament() {
		return capDeDepartament;
	}

	/**
	 * @param capDeDepartament the capDeDepartament to set
	 */
	public void setCapDeDepartament(Usuari capDeDepartament) {
		this.capDeDepartament = capDeDepartament;
	}

	/**
	 * @return the llistaPersonal
	 */
	public ArrayList<Usuari> getLlistaPersonal() {
		return llistaPersonal;
	}

	/**
	 * @param llistaPersonal the llistaPersonal to set
	 */
	public void setLlistaPersonal(ArrayList<Usuari> llistaPersonal) {
		this.llistaPersonal = llistaPersonal;
	}

}
