package exercici_05_a_07;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class Controlador {
	
	List<Usuari> llistaUsuaris;
	List<Departament> llistaDepartaments;

	@RequestMapping("/")
	public String inici() {
		// Afegim els valors a les llistes
		this.llistaUsuaris = UsAplicacio.inicialitzarUsuari();
		this.llistaDepartaments = UsAplicacio.inicialitzarDepartament();
		
		System.out.println("Llista d'Usuaris:");
		
		// Mostrem la llista d'usuaris
		for (Usuari usuari : llistaUsuaris) {
			System.out.println("Nom: " + usuari.getNom());
			System.out.println("Dni: " + usuari.getDni());
			System.out.println();
		}
		
		System.out.println("Llista de departaments:");
		
		// Mostrem la llista de departaments
		for (Departament departament : llistaDepartaments) {
			System.out.println("Id departament: " + departament.getId());
			System.out.println("Nom departament: " + departament.getNom());
			System.out.println();
		}
		
		return "inici";
	}
	
	@RequestMapping("/login")
	public String login() {
		return "login";
	}
	
	@RequestMapping(value = "/loginUsuari", method = RequestMethod.POST)
	public String loginUsuari(HttpServletRequest request, Model model) {
		// Guardem els valors del formulari
		String dniUsuari = request.getParameter("dniUsuari");
		String passwdUsuari = request.getParameter("passwdUsuari");
		
		// Comprobem que el dni està a la llista d'usuaris
		for (Usuari usuari : llistaUsuaris) {
			if (usuari.getDni().equals(dniUsuari)) {
				// Si el dni coincideix mirem la contrasenya
				if (usuari.getContrasenya().equals(passwdUsuari)) {
					// Si la contrasenya també coincideix mostrem la vista de modificar dades
					return "modificarDadesUsuari";
				} else {
					// Si la contrasenya no coincideix mostrem missatge d'error
					System.out.println("La contrasenya no és correcta");
					return "login";
				}
			}
		}
		
		// Si no ha trobat cap coincidència de dni mostrem missatge d'error
		System.out.println("El DNI no és correcte");		
		return "login";
	}
	
	@RequestMapping("/donarAlta")
	public String donarAlta() {
		return "donarAlta";
	}
	
	@RequestMapping(value = "/crearUsuari", method = RequestMethod.POST)
	public String crearTripulant(HttpServletRequest request, Model model) {
		Usuari nouUsuari;
		// Guardem els valors del formulari
		String dniUsuari = request.getParameter("dniUsuari");
		String nomUsuari = request.getParameter("nomUsuari");
		String passwdUsuari = request.getParameter("passwdUsuari");
		int departamentUsuari = Integer.parseInt(request.getParameter("departament"));
		LocalDate dataCreacioUsuari = LocalDate.now();
		
		System.out.println(dniUsuari);
		
		// Mirem que el dni de l'usuari no existeixi
		for (Usuari usuari : llistaUsuaris) {
			if (usuari.getDni().equals(dniUsuari)) {
				System.out.println("Error l'usuari amb dni " + dniUsuari + " ja existeix");
				return "donarAlta";
			}
		}
		
		// En cas que no existeixi el creem
		nouUsuari = new Usuari(dniUsuari, nomUsuari, passwdUsuari, departamentUsuari, dataCreacioUsuari);
		this.llistaUsuaris.add(nouUsuari);
		System.out.println("Usuari amb dni " + dniUsuari + " creat amb èxit");
		/*
		for (Usuari usuari : llistaUsuaris) {
			System.out.println("Dni: " + usuari.getDni());
			System.out.println("Nom: " + usuari.getNom());
			System.out.println();
		}
		*/
		return "login";
	}
	
	@RequestMapping("/modificarDades")
	public String modificarDades() {
		return "modificarDadesUsuari";
	}

	
}
