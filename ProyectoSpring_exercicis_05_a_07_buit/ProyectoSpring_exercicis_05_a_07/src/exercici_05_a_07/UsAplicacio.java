package exercici_05_a_07;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class UsAplicacio {

	public static List<Usuari> inicialitzarUsuari() {
		
		AnnotationConfigApplicationContext contexteAmbClasseConfig = new AnnotationConfigApplicationContext(aplicacioConfig.class);
		
		CreacioUsuari creacioUsuari = contexteAmbClasseConfig.getBean("creacioUsuari", CreacioUsuari.class);
		
		List<String> llistaDniUsuaris = (List<String>) Arrays.asList("43568322C", "12345678D", "87654321K");
		List<String> llistaNomsUsuaris = (List<String>) Arrays.asList("Ignasi", "Dani", "David");
		List<String> llistaContrasenyesUsuaris = (List<String>) Arrays.asList("ignasi", "dani", "david");
		List<Integer> llistaDepartamentsUsuaris = (List<Integer>) Arrays.asList(1, 2, 1);
		List<LocalDate> llistaCreacioUsuaris = (List<LocalDate>) Arrays.asList(LocalDate.now(), LocalDate.now(), LocalDate.now());
		
		creacioUsuari.creacioUsuaris(llistaDniUsuaris, llistaNomsUsuaris, llistaContrasenyesUsuaris, llistaDepartamentsUsuaris, llistaCreacioUsuaris);
		
		contexteAmbClasseConfig.close();
		
		return creacioUsuari.getUsuaris();
		// return null;
		
	}
	
	public static List<Departament> inicialitzarDepartament() {
			
		AnnotationConfigApplicationContext contexteAmbClasseConfig = new AnnotationConfigApplicationContext(aplicacioConfig.class);
		
		CreacioDepartament creacioDepartament = contexteAmbClasseConfig.getBean("creacioDepartament", CreacioDepartament.class);
		
		List<Integer> llistaNumDepartaments = (List<Integer>) Arrays.asList(1, 2, 3);
		List<String> llistaNomsDepartaments = (List<String>) Arrays.asList("Front-End", "Back-End", "Neteja");
		
		creacioDepartament.creacioDepartaments(llistaNumDepartaments, llistaNomsDepartaments);
		
		contexteAmbClasseConfig.close();
		
		return creacioDepartament.getDepartaments();
		// return null;
		
	}

}
