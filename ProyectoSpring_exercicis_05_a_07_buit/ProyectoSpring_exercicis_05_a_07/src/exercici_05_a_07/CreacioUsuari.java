package exercici_05_a_07;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CreacioUsuari {
	
	private List<Usuari> llistaUsuaris;
	private final BeanFactory factory;
	
	@Autowired
	public CreacioUsuari(final BeanFactory f) {
		this.factory = f;
	}
	
	public void creacioUsuaris(List<String> llistaDniUsuaris, List<String> llistaNomsUsuaris, 
			List<String>llistaContrasenyesUsuaris, List<Integer> llistaDepartamentsUsuaris, List<LocalDate> llistaCreacioUsuaris) {
	
		this.llistaUsuaris = llistaDniUsuaris.stream().map(param -> factory.getBean(Usuari.class, param))
				.collect(Collectors.toList());
	
		
		int index = 0;
		for (Usuari usuari : this.llistaUsuaris) {
			usuari.setNom(llistaNomsUsuaris.get(index));
			usuari.setContrasenya(llistaContrasenyesUsuaris.get(index));
			usuari.setDataCreacio(llistaCreacioUsuaris.get(index));
			usuari.setDepartament(llistaDepartamentsUsuaris.get(index));
			
			index++;
		}
	}
	
	public List<Usuari> getUsuaris() {
		return this.llistaUsuaris;
	}

}
