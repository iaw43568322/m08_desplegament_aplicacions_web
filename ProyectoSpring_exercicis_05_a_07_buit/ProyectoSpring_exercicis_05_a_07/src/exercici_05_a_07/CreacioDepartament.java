package exercici_05_a_07;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CreacioDepartament {
	
	private List<Departament> llistaDepartaments;
	private final BeanFactory factory;
	
	@Autowired
	public CreacioDepartament(final BeanFactory f) {
		this.factory = f;
	}
	
	public void creacioDepartaments(List<Integer> llistaNumDepartaments, List<String> llistaNomsDepartaments) {
	
		this.llistaDepartaments = llistaNumDepartaments.stream().map(param -> factory.getBean(Departament.class, param))
				.collect(Collectors.toList());
	
		
		int index = 0;
		for (Departament departament : this.llistaDepartaments) {
			departament.setNom(llistaNomsDepartaments.get(index));
			
			index++;
		}
	}
	
	public List<Departament> getDepartaments() {
		return this.llistaDepartaments;
	}
}
