package exercici_05_a_07;

import java.util.ArrayList;

public class Organitzacio {
	
	// ATRIBUTS
	private String nom;
	private ArrayList<Departament> llistaDepartaments;
	
	// CONSTRUCTOR
	
	/**
	 * @param nom
	 * @param llistaDepartaments
	 */
	public Organitzacio(String nom, ArrayList<Departament> llistaDepartaments) {
		super();
		this.nom = nom;
		this.llistaDepartaments = llistaDepartaments;
	}
	
	// GETTERS & SETTERS
	
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the llistaDepartaments
	 */
	public ArrayList<Departament> getLlistaDepartaments() {
		return llistaDepartaments;
	}
	/**
	 * @param llistaDepartaments the llistaDepartaments to set
	 */
	public void setLlistaDepartaments(ArrayList<Departament> llistaDepartaments) {
		this.llistaDepartaments = llistaDepartaments;
	}
	
	

}
