<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@page import="exercici_05_a_07.Usuari" %>    

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Modificar dades</title>
</head>
<body>
	<h3>Modificar Dades Usuari</h3>
	<br>
	<form action="login" method="post" modelAttribute="usuari">
		<label for="dniUsuari">DNI usuari: </label>
		<input type="text" id="dniUsuari" name="dniUsuari" value="exemple">
		<br><br>
		<label for="nomUsuari">Nom usuari: </label>
		<input type="text" id="nomUsuari" name="nomUsuari" value="exemple">
		<br><br>
		<label for="passwdUsuari">Contrasenya: </label>
		<input type="text" id="passwdUsuari" name="passwdUsuari">
		<br><br>
		<label>Departament usuari: </label>
		<br>
		<label>Departament 1</label>
		<input type="radio" id="dep1" name="departament" value="departament_1">
		<br>
		<label>Departament 2</label>
		<input type="radio" id="dep2" name="departament" value="departament_2">
		<br>
		<label>Departament 3</label>
		<input type="radio" id="dep3" name="departament" value="departament_3">
		<br><br>
		<input type="submit" value="submit">
	</form>
</body>
</html>