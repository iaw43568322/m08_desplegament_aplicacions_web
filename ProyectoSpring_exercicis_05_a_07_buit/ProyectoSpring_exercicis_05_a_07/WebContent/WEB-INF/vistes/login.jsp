<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Login</title>
</head>
<body>
	<h3>Login Usuari</h3>
	<br>
	<form action="loginUsuari" method="post" modelAttribute="usuari">
		<label for="dniUsuari">DNI usuari: </label>
		<input type="text" id="dniUsuari" name="dniUsuari">
		<br>
		<label for="passwdUsuari">Contrasenya: </label>
		<input type="passwd" id="passwdUsuari" name="passwdUsuari">
		<br><br>
		<input type="submit" value="login">
	</form>
	<br>
	<a href="donarAlta">Donar d'alta nou usuari</a>
</body>
</html>