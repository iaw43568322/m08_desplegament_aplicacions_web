<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Alta usuari</title>
</head>
<body>
	<h3>Donar d'alta nou usuari</h3>
	<br>
	<form action="crearUsuari" method="post" modelAttribute="usuari">
		<label for="dniUsuari">DNI usuari: </label>
		<input type="text" id="dniUsuari" name="dniUsuari">
		<br><br>
		<label for="nomUsuari">Nom usuari: </label>
		<input type="text" id="nomUsuari" name="nomUsuari">
		<br><br>
		<label>Id Departament: </label>
		<br>
		<input type="radio" id="dep1" name="departament" value="1">
		<label for="dep1">1</label>
		<br>
		<input type="radio" id="dep2" name="departament" value="2">
		<label for="dep2">2</label>
		<br>
		<input type="radio" id="dep3" name="departament" value="3">
		<label for="dep3">3</label>
		<br><br>
		<label for="passwdUsuari">Contrasenya usuari: </label>
		<input type="text" id="passwdUsuari" name="passwdUsuari">
		<br><br>
		<input type="submit" value="Donar d'alta">
	</form>
</body>
</html>